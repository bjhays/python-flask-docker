#!/bin/bash

DF="docker/Dockerfile"
PROJECT=flasktest
LPORT=5150
BREAK="####################################"

docker build $CACHE -f "$DF" -t "$PROJECT" .

echo $BREAK
echo "Starting container with port $LPORT"
echo $BREAK
docker run -p $LPORT:5000 -ti -v $(pwd):/app "$PROJECT" 
